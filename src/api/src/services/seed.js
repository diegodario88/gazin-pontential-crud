const faker = require("faker");
const Developer = require("../models/developer.model");
const Proficiency = require("../models/proficiency.model");

faker.locale = "pt_BR";

const skills = [
  "JavaScript",
  "PHP",
  "NodeJs",
  "C#",
  "Java",
  "HTML",
  "CSS",
  "HTML",
  "GIT",
  "AWS",
  "SQL",
  "NoSQL",
  "Python",
  "Delphi",
  "SqlServer",
  "Android",
  "Linux",
  "Testing",
  "Bootstrap",
  "Laravel",
  "Express",
  "Docker",
  "NextJS",
  "React",
  "Angular",
  "React Native",
  "GitHub",
  "Azure",
  "Redis",
  "Mongo",
  "PostgreSQL",
  "CodeIgniter",
  "Google Cloud Platform",
  "GoLang",
  "Rust",
  "Swift",
  "Flutter",
  "Jest",
  "Object-Oriented Design",
  "Function Design",
  "Teamwork",
  "Communication",
];

function getRandomSkillIndex() {
  return faker.datatype.number({
    min: 0,
    max: skills.length - 1,
  });
}

function chooseRandomUnique(developerSkills) {
  const choose = skills[getRandomSkillIndex()];
  const foundInPickedSkills = developerSkills.find((item) => item === choose);

  if (typeof foundInPickedSkills === "undefined") {
    return choose;
  }

  return chooseRandomUnique(developerSkills);
}

function getAge(dateString) {
  const today = new Date();
  const birthDate = new Date(dateString);
  let age = today.getFullYear() - birthDate.getFullYear();
  const m = today.getMonth() - birthDate.getMonth();

  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  return age;
}

async function seedUsers() {
  try {
    const count = await Developer.countDocuments();

    if (count > 1) {
      return;
    }

    const quantity = 50;
    const developers = [];

    for (let index = 0; index < quantity; index++) {
      const skillIteration = faker.datatype.number({
        min: 3,
        max: 8,
      });

      const developerSkills = [];

      for (let index = 0; index < skillIteration; index++) {
        const choose = chooseRandomUnique(developerSkills);
        developerSkills.push(choose);
      }

      const proficiencies = developerSkills.map(
        (skill) =>
          new Proficiency({
            skill: skill,
            rate: faker.datatype.number({
              min: 1,
              max: 100,
            }),
          })
      );

      await Proficiency.insertMany(proficiencies);

      const fakeDate = faker.date.between("1970-01-01", "2002-01-01");

      developers.push(
        new Developer({
          name: `${faker.name.firstName()} ${faker.name.lastName()}`,
          gender: faker.name.gender(),
          birthday: fakeDate,
          age: getAge(fakeDate),
          hobby: faker.random.word(),
          email: faker.internet.email(),
          bio: faker.lorem.paragraph(),
          proficiency: proficiencies,
        })
      );
    }

    const diegoProficiency = [
      new Proficiency({
        skill: "JavaScript",
        rate: 90,
      }),
      new Proficiency({
        skill: "HTML",
        rate: 78,
      }),
      new Proficiency({
        skill: "CSS",
        rate: 71,
      }),
      new Proficiency({
        skill: "Linux",
        rate: 75,
      }),
      new Proficiency({
        skill: "PHP",
        rate: 70,
      }),
      new Proficiency({
        skill: "NodeJs",
        rate: 83,
      }),
    ];

    await Proficiency.insertMany(diegoProficiency);

    const diegoDev = new Developer({
      name: "Diego Dario",
      gender: "Man",
      age: 32,
      hobby:
        "Leitura (Ficção de aventura, Fantasia heroica, Romance de cavalaria),  Caminhar com os dogs 🐺",
      email: "diegodario88@gmail.com",
      bio: "Pai do Raul; Desenvolvedor; Amante da Tecnologia; Eterno aluno",
      birthday: "1988-10-11:06:00.000Z",
      avatar: "https://avatars.githubusercontent.com/u/25825145?v=4",
      proficiency: diegoProficiency,
    });

    developers.push(diegoDev);

    await Developer.insertMany(developers);

    console.log("Database seeded! 😄");
  } catch (error) {
    console.error(error);
  }
}

module.exports = { seedUsers };
