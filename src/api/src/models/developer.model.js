const mongoose = require("mongoose");

const developerSchema = new mongoose.Schema(
  {
    name: {
      type: "String",
      minlength: 4,
      maxlength: 80,
      required: true,
    },
    gender: {
      type: "String",
      required: true,
    },
    age: {
      type: "Number",
      min: [18, "Must be at least 18, got {VALUE}"],
      required: true,
    },
    hobby: {
      type: "String",
    },
    email: {
      type: "String",
      unique: true,
      required: true,
    },
    bio: {
      type: "String",
    },
    birthday: {
      type: "Date",
      required: true,
    },
    avatar: {
      type: "String",
    },
    proficiency: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Proficiency",
        required: true,
      },
    ],
  },
  { timestamps: true }
);

const Developer = mongoose.model("Developer", developerSchema);

module.exports = Developer;
