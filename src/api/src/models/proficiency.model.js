const mongoose = require("mongoose");

const proficiencySchema = new mongoose.Schema(
  {
    skill: {
      type: "String",
      minlength: 2,
      maxlength: 80,
      required: true,
    },
    rate: {
      type: "Number",
      min: [1, "Must be at least 18, got {VALUE}"],
      max: [100, "Must be maximum 100, got {VALUE}"],
      required: true,
    },
  },
  { timestamps: true }
);

const Proficiency = mongoose.model("Proficiency", proficiencySchema);

module.exports = Proficiency;
