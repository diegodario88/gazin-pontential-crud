const { Router } = require("express");
const Controller = require("../handlers/controller");

const router = Router();

router.all("/", Controller.index);
router.get("/api/v1/developers", Controller.getAll);
router.get("/api/v1/developers/:id", Controller.getById);
router.get("/api/v1/developer", Controller.getByEmail);
router.post("/api/v1/developers", Controller.store);
router.put("/api/v1/developers/:id", Controller.edit);
router.delete("/api/v1/developers/:id", Controller.destroy);

module.exports = router;
