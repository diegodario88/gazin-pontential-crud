const Developer = require("../models/developer.model");
const Proficiency = require("../models/proficiency.model");
const { isEmpty } = require("../utils/helpers");

async function index(req, res) {
  res.status(200).send({
    title: "Gazin  Api",
    description: "Fornece informações sobre desenvolvedores",
    route: "/api/v1/developers",
    version: "1.0.0",
    author: "Diego Dario",
  });
}

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
async function getAll(req, res) {
  try {
    const page = Number(req.query.page) || 1;
    const limit = Number(req.query.limit) || 10;
    const head = (page - 1) * limit;
    const tail = page * limit;
    const next = {};
    const previous = {};
    const count = await Developer.countDocuments();

    if (tail < count) {
      next.page = page + 1;
    }

    if (head > 0) {
      previous.page = page - 1;
    }

    const developers = await Developer.find()
      .populate("proficiency")
      .limit(limit * 1)
      .skip((page - 1) * limit)
      .exec();

    const data = {
      developers,
      totalPages: Math.ceil(count / limit),
      currentPage: page,
      previous,
      next,
      limit,
      count,
    };

    return res.status(200).json(data);
  } catch (error) {
    console.error("Are we up? 🤔", error.message);
    return res.status(400).send();
  }
}

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
async function getByEmail(req, res) {
  const email = req.query.email;

  try {
    const developer = await Developer.findOne({ email }).populate(
      "proficiency"
    );

    if (isEmpty(developer)) {
      console.error("Bad request! developer do not exist");
      return res.status(400).send();
    }

    return res.status(200).json(developer);
  } catch (error) {
    console.error(
      `No developer found, probably id: ${email} did not match.`,
      error.message
    );
    return res.status(204).send();
  }
}

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
async function getById(req, res) {
  const { id } = req.params;

  try {
    const developer = await Developer.findById(id).populate("proficiency");

    return res.status(200).json(developer);
  } catch (error) {
    console.error(
      `No developer found, probably id: ${id} did not match.`,
      error.message
    );
    return res.status(204).send();
  }
}

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
async function store(req, res) {
  try {
    const dataDeveloper = req.body;
    const { proficiency } = dataDeveloper;

    const proficiencies = proficiency.map((skill) => new Proficiency(skill));

    const developer = new Developer({
      name: dataDeveloper.name,
      gender: dataDeveloper.gender,
      birthday: dataDeveloper.birthday,
      age: dataDeveloper.age,
      hobby: dataDeveloper.hobby,
      email: dataDeveloper.email,
      bio: dataDeveloper.bio,
      avatar: dataDeveloper.avatar,
      proficiency: proficiencies,
    });

    await Proficiency.create(proficiencies);
    const developerResult = await Developer.create(developer);

    return res.status(201).json(developerResult);
  } catch (error) {
    console.error("Wrong inputs, cannot store that data ☠️.", error.message);
    return res.status(400).send();
  }
}

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
async function edit(req, res) {
  const { id } = req.params;

  try {
    const dataDeveloper = req.body;
    const { proficiency } = dataDeveloper;
    const proficiencies = proficiency.map((skill) => new Proficiency(skill));

    const developer = {
      name: dataDeveloper.name,
      gender: dataDeveloper.gender,
      birthday: dataDeveloper.birthday,
      age: dataDeveloper.age,
      hobby: dataDeveloper.hobby,
      email: dataDeveloper.email,
      bio: dataDeveloper.bio,
      avatar: dataDeveloper.avatar,
      proficiency: proficiencies,
    };

    await Proficiency.create(proficiencies);

    const developerAfterUpdate = await Developer.findByIdAndUpdate(
      id,
      developer,
      {
        new: true,
        runValidators: true,
      }
    );

    return res.status(200).json(developerAfterUpdate);
  } catch (error) {
    console.error("Wrong inputs, cannot update that data ☠️.", error.message);
    return res.status(400).send();
  }
}

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
async function destroy(req, res) {
  const { id } = req.params;

  try {
    const developer = await Developer.findByIdAndDelete(id);

    if (isEmpty(developer)) {
      return res.status(404).json({
        message: "Developer not found, maybe it's already destroyed.",
      });
    }

    return res.status(200).json(developer);
  } catch (error) {
    console.error("Wrong inputs, cannot delete that data ☠️.", error.message);
    return res.status(400).send();
  }
}

module.exports = { index, getAll, getByEmail, getById, store, edit, destroy };
