const express = require("express");
const cors = require("cors");
const helmet = require("helmet");
const mongo = require("mongoose");
const routes = require("./routes/router");
const { seedUsers } = require("./services/seed");

const PORT = 8080;
const DB_URL = "mongodb://devuser:devpass@mongodb/gazin";
const DB_CONFIG = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
};
const app = express();

app.use(cors());
app.use(helmet());
app.use(express.json());
app.use(routes);

app.listen(PORT, () => console.log(`Api Running at port:...${PORT}`));

mongo.connect(DB_URL, DB_CONFIG, () => console.log("Mongodb connected!"));

(async () => await seedUsers())();
