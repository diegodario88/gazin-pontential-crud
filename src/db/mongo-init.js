db.auth("admin-user", "admin-password");

db = db.getSiblingDB("gazin");

db.createUser({
  user: "devuser",
  pwd: "devpass",
  roles: ["readWrite"],
});
