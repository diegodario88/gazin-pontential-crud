import React from 'react';
import Typography from '@material-ui/core/Typography';
import { DeveloperContext } from '../developer/context';
import {
  ResponsiveContainer,
  RadarChart,
  PolarGrid,
  PolarAngleAxis,
  PolarRadiusAxis,
  Radar
} from 'recharts';
import Progress from '../loading/progress';

export default function Chart() {
  const { state } = React.useContext(DeveloperContext);

  return (
    <React.Fragment>
      <Typography
        style={{ marginLeft: '10px' }}
        component="h2"
        variant="h6"
        color="primary"
      >
        Proficiency
      </Typography>
      <ResponsiveContainer width="100%" height="100%">
        {state.isLoading ? (
          <Progress />
        ) : (
          <RadarChart
            cx="50%"
            cy="50%"
            outerRadius="80%"
            data={state?.proficiency}
          >
            <PolarGrid />
            <PolarAngleAxis dataKey="skill" tick={true} />
            <PolarRadiusAxis />
            <Radar
              name="CodeRadar"
              dataKey="rate"
              stroke="#8884d8"
              fill="#8884d8"
              fillOpacity={0.6}
            />
          </RadarChart>
        )}
      </ResponsiveContainer>
    </React.Fragment>
  );
}
