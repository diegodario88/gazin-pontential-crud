import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { useLocation } from 'react-router-dom';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import DashboardIcon from '@material-ui/icons/Dashboard';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import Contact from '@material-ui/icons/Contacts';
import Code from '@material-ui/icons/Code';
import Info from '@material-ui/icons/Info';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  dividerSpace: {
    marginBottom: 24,
    marginTop: 24
  },
  dashBoardIconSpace: {
    marginTop: 8
  }
}));
export default function DrawerItems() {
  const classes = useStyles();
  const location = useLocation();

  return (
    <div className={classes.root}>
      <Divider />
      <Link to="/" style={{ color: 'inherit', textDecoration: 'inherit' }}>
        <ListItem
          className={classes.dashBoardIconSpace}
          button
          selected={location.pathname === '/'}
        >
          <ListItemIcon>
            <DashboardIcon />
          </ListItemIcon>
          <ListItemText primary="Dashboard" />
        </ListItem>
      </Link>
      <Link
        to="/register"
        style={{ color: 'inherit', textDecoration: 'inherit' }}
      >
        <ListItem button selected={location.pathname === '/register'}>
          <ListItemIcon>
            <PersonAddIcon />
          </ListItemIcon>
          <ListItemText primary="Register" />
        </ListItem>
      </Link>
      <Link to="/code" style={{ color: 'inherit', textDecoration: 'inherit' }}>
        <ListItem button selected={location.pathname === '/code'}>
          <ListItemIcon>
            <Code />
          </ListItemIcon>
          <ListItemText primary="Code" />
        </ListItem>
      </Link>

      <Link
        to="contact"
        style={{ color: 'inherit', textDecoration: 'inherit' }}
      >
        <ListItem button selected={location.pathname === '/contact'}>
          <ListItemIcon>
            <Contact />
          </ListItemIcon>
          <ListItemText primary="Contact" />
        </ListItem>
      </Link>

      <Link to="/info" style={{ color: 'inherit', textDecoration: 'inherit' }}>
        <ListItem button selected={location.pathname === '/info'}>
          <ListItemIcon>
            <Info />
          </ListItemIcon>
          <ListItemText primary="Info" />
        </ListItem>
      </Link>
    </div>
  );
}
