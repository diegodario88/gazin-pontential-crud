import * as React from 'react';
import { DataGrid } from '@material-ui/data-grid';
import Typography from '@material-ui/core/Typography';
import Progress from '../loading/progress';
import http from '../../services/http';
import { DeveloperContext } from './context';
import { sleep } from '../../utils/helpers';

const columnsOptions = [
  {
    field: '_id',
    headerName: 'Identifier',
    minWidth: 150,
    valueFormatter: ({ value }) => value.substring(15, 24)
  },
  { field: 'name', headerName: 'Name', minWidth: 150 },
  { field: 'age', headerName: 'Age', minWidth: 150 },
  { field: 'gender', headerName: 'Gender', minWidth: 270 },
  { field: 'hobby', headerName: 'Hobby', minWidth: 150 },
  {
    field: 'birthday',
    headerName: 'Birthday',
    minWidth: 150,
    valueFormatter: ({ value }) => new Date(value).toLocaleDateString()
  },
  {
    field: 'createdAt',
    headerName: 'Joined',
    minWidth: 150,
    valueFormatter: ({ value }) => new Date(value).toLocaleDateString()
  }
];

async function loadDevelopers(page, limit) {
  const { data } = await http.get(`developers?page=${page}&limit=${limit}`);
  return data;
}

export default function ServerPaginationGrid() {
  const [page, setPage] = React.useState(1);
  const [limit, setLimit] = React.useState(5);
  const [loading, setLoading] = React.useState(true);
  const [data, setData] = React.useState(null);
  const { dispatch } = React.useContext(DeveloperContext);

  const handleDeveloperSelection = ({ row }) =>
    dispatch({
      type: 'UPDATE_DEVELOPER',
      payload: { ...row, isLoading: false }
    });

  React.useEffect(() => {
    let active = true;

    (async () => {
      setLoading(true);
      const newData = await loadDevelopers(page, limit);

      if (!active) {
        return;
      }

      sleep(500).then(() => {
        //Simular latência de rede
        setData(newData);
        setLoading(false);
      });
    })();

    return () => {
      active = false;
    };
  }, [page, limit]);

  if (!data) {
    return <Progress />;
  }

  return (
    <div style={{ height: 400, width: '100%' }}>
      <Typography component="h2" variant="h6" color="primary" gutterBottom>
        Developers
      </Typography>
      <DataGrid
        rows={data?.developers}
        getRowId={(row) => row._id}
        columns={columnsOptions}
        pagination
        pageSize={data?.limit}
        rowCount={data?.count}
        paginationMode="server"
        onPageChange={(newPage) => {
          if (newPage === page) {
            setPage(newPage + 1);
            return;
          }
          setPage(newPage + 1);
        }}
        loading={loading}
        rowsPerPageOptions={[5, 10, 25, 50]}
        onPageSizeChange={(newLimit) => setLimit(newLimit)}
        onCellClick={handleDeveloperSelection}
      />
    </div>
  );
}
