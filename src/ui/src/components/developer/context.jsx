import React from 'react';
import http from '../../services/http';
import { sleep } from '../../utils/helpers';
import { useLocation } from 'react-router-dom';

const INITIAL_STATE = {
  isLoading: true
};

function reducer(state, action) {
  switch (action.type) {
    case 'UPDATE_DEVELOPER':
      return {
        ...action.payload
      };
    default:
      throw new Error('Nenhuma ação válida foi passada para o reducer');
  }
}

export const DeveloperContext = React.createContext(INITIAL_STATE);

const DeveloperContextProvider = ({ children }) => {
  const [state, dispatch] = React.useReducer(reducer, INITIAL_STATE);
  const location = useLocation();

  React.useEffect(() => {
    (async () => {
      dispatch({
        type: 'UPDATE_DEVELOPER',
        payload: { ...INITIAL_STATE }
      });

      const { data } = await http.get('developer?email=diegodario88@gmail.com');

      sleep(1000).then(() =>
        dispatch({
          type: 'UPDATE_DEVELOPER',
          payload: { ...data, isLoading: false }
        })
      );
    })();
  }, [location]);

  return (
    <DeveloperContext.Provider value={{ state, dispatch }}>
      {children}
    </DeveloperContext.Provider>
  );
};

export default DeveloperContextProvider;
