import React from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Footer from '../footer/footer';
import Avatar from '@material-ui/core/Avatar';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Slider from '@material-ui/core/Slider';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import {
  skills,
  isAdult,
  getAge,
  sleep,
  isEmpty
} from '../../utils/helpers.js';
import http from '../../services/http';

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(3)
  },
  container: {
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(4)
  },
  boxButton: {
    marginTop: '3rem',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  avatarLabel: {
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer'
  },
  button: {
    width: theme.spacing(50)
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff'
  }
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function CenteredGrid() {
  const classes = useStyles();
  const location = useLocation();
  const history = useHistory();
  const invalid = 'Incorrect entry.';
  const isEdition = !isEmpty(location?.state);

  const [loading, setLoading] = React.useState(false);
  const [chooseSkills, setChooseSkills] = React.useState([]);
  const [rateSkills, setRateSkills] = React.useState([]);
  const [snack, setSnack] = React.useState({
    dialog: false,
    type: '',
    message: ''
  });

  const INITIAL_VALUE = {
    name: {
      error: null,
      value: isEdition ? location.state.name.split(' ')[0] : ''
    },
    lastName: {
      error: null,
      value: isEdition ? location.state.name.split(' ')[1] : ''
    },
    email: {
      error: null,
      value: isEdition ? location.state.email : ''
    },
    birthday: {
      error: null,
      value: isEdition ? location.state.birthday.substr(0, 10) : ''
    },
    bio: {
      error: null,
      value: isEdition ? location.state.bio : ''
    },
    gender: {
      error: null,
      value: isEdition ? location.state.gender : ''
    },
    hobby: {
      error: null,
      value: isEdition ? location.state.hobby : ''
    },
    avatar: {
      preview: isEdition ? location.state.avatar : '',
      base64: ''
    }
  };

  const [values, setValues] = React.useState(INITIAL_VALUE);

  const createDeveloper = async (developer) => {
    try {
      setLoading(true);
      const result = await http.post('developers', developer);

      sleep(1300).then(() => {
        setValues(INITIAL_VALUE);
        document
          .getElementsByClassName('MuiAutocomplete-clearIndicator')[0]
          .click();

        setLoading(false);
        setSnack({
          dialog: true,
          type: 'success',
          message: result.statusText
        });
      });
    } catch (error) {
      setLoading(false);
      setSnack({
        dialog: true,
        type: 'error',
        message: 'Unable to register developer'
      });
    }
  };

  const updateDeveloper = async (developer) => {
    try {
      setLoading(true);
      await http.put(`developers/${location.state._id}`, developer);
      sleep(1300).then(() => history.push('/'));
    } catch (error) {
      setLoading(false);
      setSnack({
        dialog: true,
        type: 'error',
        message: 'Unable to update developer'
      });
    }
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setSnack({ ...snack, dialog: false });
  };

  const handleChange = ({ target }) => {
    if (target.id === 'birthday') {
      if (!isAdult(target.value)) {
        handleError({ target });
        return;
      }
    }

    setValues({
      ...values,
      [target.id]: { error: null, value: target.value }
    });
  };

  const handleSubmit = (submitEvent) => {
    submitEvent.preventDefault();
    let shouldAbort = false;

    Object.values(values).forEach((value) => {
      if (value.error) {
        shouldAbort = true;
      }
    });

    if (shouldAbort) {
      setSnack({
        dialog: true,
        type: 'error',
        message: 'Form is invalid, please review'
      });
      return;
    }

    const developer = {
      name: `${values.name.value} ${values.lastName.value}`,
      gender: values.gender.value,
      age: getAge(values.birthday.value),
      hobby: values.hobby.value ?? 'Uninformed. 🙁',
      email: values.email.value,
      bio: values.bio.value ?? 'Uninformed. 🙁',
      birthday: values.birthday.value,
      avatar: values.avatar.preview,
      proficiency: rateSkills
    };

    isEdition ? updateDeveloper(developer) : createDeveloper(developer);
  };

  const handleSkillChoose = (chooseTags) => {
    const withRate = chooseTags.map((tag) => {
      const found = rateSkills.find((r) => r.skill === tag);
      if (found) {
        return found;
      }
      return {
        skill: tag,
        rate: 30
      };
    });
    setChooseSkills(chooseTags);
    setRateSkills(withRate);
  };

  const handleRateSkillChange = (rateSkill, value) => {
    const filteredSkills = rateSkills.filter(
      ({ skill }) => skill !== rateSkill
    );

    const rate = { skill: rateSkill, rate: value };
    filteredSkills.push(rate);

    setRateSkills(filteredSkills);
  };

  const handleError = ({ target }) => {
    setValues({
      ...values,
      [target.id]: { error: true, value: target.value }
    });
  };

  React.useEffect(() => {
    if (!isEdition) {
      return;
    }

    const proficiency = location.state.proficiency;
    const tags = proficiency.map((pro) => pro.skill);
    const withRate = proficiency.map((pro) => ({
      skill: pro.skill,
      rate: pro.rate
    }));
    setChooseSkills(tags);
    setRateSkills(withRate);
  }, [location.state?.proficiency, isEdition]);

  return (
    <Container maxWidth="lg" className={classes.container}>
      <Backdrop className={classes.backdrop} open={loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <Typography component="h2" variant="h6" color="primary" gutterBottom>
        Register
      </Typography>
      <form onSubmit={handleSubmit}>
        <Paper className={classes.paper}>
          <Typography
            variant="body2"
            color="textSecondary"
            component="p"
            gutterBottom
          >
            Fill this form to register a new developer
          </Typography>
          <Grid container spacing={3}>
            <Grid item xs={12} md={6}>
              <TextField
                id="name"
                onChange={handleChange}
                error={values.name.error}
                label="Name"
                onInvalid={handleError}
                value={values.name.value}
                helperText={values.name.error ? invalid : null}
                required
                fullWidth
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField
                id="lastName"
                onChange={handleChange}
                error={values.lastName.error}
                label="Last Name"
                onInvalid={handleError}
                value={values.lastName.value}
                helperText={values.lastName.error ? invalid : null}
                required
                fullWidth
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField
                id="email"
                onChange={handleChange}
                error={values.email.error}
                label="Email"
                onInvalid={handleError}
                value={values.email.value}
                helperText={values.email.error ? invalid : null}
                required
                fullWidth
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField
                id="birthday"
                onChange={handleChange}
                error={values.birthday.error}
                label="Birthday"
                onInvalid={handleError}
                value={values.birthday.value}
                helperText={
                  values.birthday.error ? `${invalid} Or isn't adult.` : null
                }
                type="date"
                required
                InputLabelProps={{
                  shrink: true
                }}
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField
                id="gender"
                onChange={handleChange}
                error={values.gender.error}
                label="Gender"
                onInvalid={handleError}
                value={values.gender.value}
                helperText={values.gender.error ? invalid : null}
                required
                fullWidth
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField
                id="hobby"
                onChange={handleChange}
                value={values.hobby.value}
                label="Hobby"
                fullWidth
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField
                id="bio"
                onChange={handleChange}
                value={values.bio.value}
                label="Bio"
                fullWidth
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <input
                accept="image/*"
                id="avatar"
                type="file"
                hidden
                onChange={(e) =>
                  setValues({
                    ...values,
                    avatar: {
                      preview: URL.createObjectURL(e.target.files[0]),
                      base64: e.target.files[0]
                    }
                  })
                }
              />
              <label htmlFor="avatar" className={classes.avatarLabel}>
                <Button component="span">
                  <Avatar variant="rounded" src={values?.avatar?.preview} />
                </Button>
                <Typography variant="body2" color="textSecondary" component="p">
                  Avatar upload
                </Typography>
              </label>
            </Grid>
            <Grid item xs={12} md={12}>
              <Autocomplete
                required
                multiple
                id="skills"
                options={skills}
                value={chooseSkills}
                freeSolo
                onChange={(_, object) => handleSkillChoose(object)}
                getOptionLabel={(option) => option}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="standard"
                    label={'Skills'}
                    helperText="Register at least three skills."
                    placeholder="Favorites"
                    error={chooseSkills.length < 3}
                    inputProps={{
                      ...params.inputProps,
                      required: chooseSkills.length < 3
                    }}
                    required={true}
                  />
                )}
              />
            </Grid>
            <Grid container spacing={3} className={classes.container}>
              {chooseSkills.map((skill, index) => (
                <Grid item xs={12} md={3} key={index}>
                  <Typography id={`rate-${skill}`} gutterBottom>
                    {skill}
                  </Typography>
                  <Slider
                    onChange={(_, value) => handleRateSkillChange(skill, value)}
                    defaultValue={30}
                    value={rateSkills.find((r) => r.skill === skill).rate}
                    aria-labelledby={`rate-${skill}`}
                    valueLabelDisplay="auto"
                    step={10}
                    marks
                    min={10}
                    max={100}
                  />
                </Grid>
              ))}
            </Grid>
          </Grid>
        </Paper>
        <Box className={classes.boxButton}>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            className={classes.button}
          >
            {isEdition ? 'FIRE EDIT PROFILE' : 'FIRE REGISTER'}
          </Button>
        </Box>
        <Snackbar
          open={snack.dialog}
          autoHideDuration={6000}
          onClose={handleClose}
        >
          <Alert onClose={handleClose} severity={snack.type}>
            {snack.message}
          </Alert>
        </Snackbar>
      </form>
      <Box pt={4}>
        <Footer />
      </Box>
    </Container>
  );
}
