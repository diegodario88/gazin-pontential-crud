import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1
  }
}));

export default function DeveloperInfo({ developer }) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={1}>
        <Grid item xs={12} sm={12}>
          <Typography variant="body2" color="textSecondary" component="p">
            {`Age: ${developer?.age}`}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12}>
          <Typography variant="body2" color="textSecondary" component="p">
            {`Gender: ${developer?.gender}`}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12}>
          <Typography variant="body2" color="textSecondary" component="p">
            {`Email: ${developer?.email}`}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12}>
          <Typography variant="body2" color="textSecondary" component="p">
            {`Birthday: ${new Date(developer?.birthday).toLocaleDateString()}`}
          </Typography>
        </Grid>
        <Grid item xs zeroMinWidth>
          <Typography variant="body2" color="textSecondary" component="p">
            {`Hobby: ${developer?.hobby}`}
          </Typography>
        </Grid>
      </Grid>
    </div>
  );
}
