import { CircularProgress, makeStyles } from '@material-ui/core';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(4)
  }
}));

export default function Progress() {
  const classes = useStyles();
  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent="center"
      minHeight="30vh"
    >
      <CircularProgress
        className={classes.root}
        size={200}
        color="primary"
        disableShrink
      />
    </Box>
  );
}
