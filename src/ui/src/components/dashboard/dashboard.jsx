import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Footer from '../footer/footer';
import DeveloperCard from '../developer/card';
import Chart from '../chart/chart';
import { useStyles } from '../../styles/global';
import DeveloperGrid from '../developer/grid';

export default function Dashboard() {
  const classes = useStyles();

  return (
    <Container maxWidth="lg" className={classes.container}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={4} lg={3}>
          <DeveloperCard />
        </Grid>
        <Grid item xs={12} md={8} lg={9}>
          <Paper>
            <Chart />
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <DeveloperGrid />
        </Grid>
      </Grid>
      <Box pt={4}>
        <Footer />
      </Box>
    </Container>
  );
}
