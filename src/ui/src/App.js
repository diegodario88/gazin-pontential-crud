import React from 'react';
import Drawer from './components/drawer/drawer';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import DeveloperContextProvider from './components/developer/context';
import { withRouter } from 'react-router-dom';
import { createTheme, ThemeProvider } from '@material-ui/core/styles';
import { ptBR } from '@material-ui/core/locale';

function App() {
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');

  const theme = React.useMemo(
    () =>
      createTheme(
        {
          palette: {
            type: prefersDarkMode ? 'dark' : 'light'
          }
        },
        ptBR
      ),
    [prefersDarkMode]
  );

  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <DeveloperContextProvider>
          <Drawer />
        </DeveloperContextProvider>
      </div>
    </ThemeProvider>
  );
}

export default withRouter(App);
