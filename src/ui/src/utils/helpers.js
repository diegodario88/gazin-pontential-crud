/**
 * @param {number} ms Milissegundos dormindo
 * @returns {Promise} Promessa
 */
export const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

export const skills = [
  'JavaScript',
  'PHP',
  'NodeJs',
  'C#',
  'Java',
  'HTML',
  'CSS',
  'HTML',
  'GIT',
  'AWS',
  'SQL',
  'NoSQL',
  'Python',
  'Delphi',
  'SqlServer',
  'Android',
  'Linux',
  'Testing',
  'Bootstrap',
  'Laravel',
  'Express',
  'Docker',
  'NextJS',
  'React',
  'Angular',
  'React Native',
  'GitHub',
  'Azure',
  'Redis',
  'Mongo',
  'PostgreSQL',
  'CodeIgniter',
  'Google Cloud Platform',
  'GoLang',
  'Rust',
  'Swift',
  'Flutter',
  'Jest',
  'Object-Oriented Design',
  'Function Design',
  'Teamwork',
  'Communication'
];

export function isAdult(birthDate) {
  const birthDateArray = birthDate.split('-');
  const day = Number(birthDateArray[2]);
  const month = Number(birthDateArray[1]) - 1;
  const year = Number(birthDateArray[0]);

  const borndate = new Date(year, month, day);
  const bornDay = borndate.getDay();
  const bornMonth = borndate.getMonth();
  const bornYear = borndate.getFullYear();

  const now = new Date(Date.now());
  const nowDay = now.getDay();
  const nowMonth = now.getMonth();
  const nowYear = now.getFullYear();

  let yearDiff = nowYear - bornYear;

  if (nowMonth < bornMonth || (nowMonth == bornMonth && nowDay < bornDay)) {
    --yearDiff;
  }

  return yearDiff >= 18;
}

export const getAge = (dateString) => {
  const today = new Date();
  const birthDate = new Date(dateString);
  let age = today.getFullYear() - birthDate.getFullYear();
  const m = today.getMonth() - birthDate.getMonth();

  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  return age;
};

export const isEmpty = (obj) => {
  const isNullOrUndefined = obj === null || obj === undefined;
  if (isNullOrUndefined) {
    return true;
  }
  return Object.keys(obj).length === 0;
};
